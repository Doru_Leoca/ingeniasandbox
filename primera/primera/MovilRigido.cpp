#include "StdAfx.h"
#include "MovilRigido.h"

MovilRigido::MovilRigido(void)
	: Movil(3.0, 2.0)
{
}

MovilRigido::~MovilRigido(void) 
{
}


void MovilRigido::simular(double inc_t) 
{
	Movil::simular(inc_t);
	x = this->x + 3*inc_t;
}
